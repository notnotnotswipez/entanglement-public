﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Entanglement.Extensions;
using UnityEngine;

using MelonLoader;

namespace Entanglement.Managers
{
    [RegisterTypeInIl2Cpp]
    public class RagdollBehaviour : MonoBehaviour
    {
        public RagdollBehaviour(IntPtr intPtr) : base(intPtr) { }

        public Rigidbody[] rbs;
        private GameObject playerModel;

        private bool hasPlayerModel = false;

        private Transform ragdollSkull;
        private Transform playerModelSHJnt;

        private float fixedTime;
        private bool isDespawning;

        private Dictionary<Transform, Transform> fromToTransforms = new Dictionary<Transform, Transform>();

        public void Start() {
            rbs = GetComponentsInChildren<Rigidbody>(true);
            fixedTime = Time.fixedTime;
            
        }

        public void SetPlayerModel(GameObject modelPlayer)
        {
            // Basically just moves the bones on the PM to the matching bones on the ragdoll, thankfully, computers are fast and can
            // Loop through large amounts of data without performance impact. There may be a more elegant way to do this ragdollified PM, but this works pretty dang well.
            hasPlayerModel = true;
            ragdollSkull = getSpecificChild(transform, "skull");
            playerModel = Instantiate(modelPlayer);
            playerModelSHJnt = getSpecificChild(playerModel.transform, "SHJntGrp");
            Transform geoGrp = getSpecificChild(transform, "geoGrp");
            MapAllRelativeBones(ragdollSkull);
            
            geoGrp.gameObject.active = false;
        }

        private void MapAllRelativeBones(Transform parent)
        {
            Transform[] transforms = parent.GetComponentsInChildren<Transform>();
            foreach (var child in transforms)
            {
                // Is a bone
                if (fromToTransforms.Keys.Contains(child)) continue;

                if (child.gameObject.name.Contains("SHJnt"))
                {
                    Transform relativeTransform = getSpecificChild(playerModelSHJnt, child.gameObject.name);
                    if (relativeTransform != null)
                    {
                        fromToTransforms.Add(child, relativeTransform);
                    }
                }
            }
        }

        private Transform getSpecificChild(Transform parent, String name)
        {
            Transform[] transforms = parent.GetComponentsInChildren<Transform>();
            foreach (var child in transforms)
            {
                if (child.gameObject.name.Contains(name))
                {
                    return child;
                }
            }

            return null;
        }

        public void FixedUpdate() {
            if (hasPlayerModel)
            {
                foreach (Transform ragdollBone in fromToTransforms.Keys)
                {
                    Transform playermodelBone;
                    bool found = fromToTransforms.TryGetValue(ragdollBone, out playermodelBone);
                    if (found)
                    {
                        playermodelBone.position = ragdollBone.position;
                        playermodelBone.rotation = ragdollBone.rotation;
                        playermodelBone.localScale = ragdollBone.localScale;
                    }
                }
            }

            if (isDespawning)
                return;

            if (Time.fixedTime - fixedTime >= 30f) {
                isDespawning = true;
                MelonCoroutines.Start(Despawn());
            }
        }

        public IEnumerator Despawn() {
            transform.position = transform.GetChild(0).position;
            transform.GetChild(0).localPosition = Vector3.zero;

            foreach (Rigidbody rb in rbs) {
                rb.isKinematic = true;
                rb.detectCollisions = false;
            }

            float elapsed = 0f;
            while (elapsed < 1f) {
                elapsed += Time.deltaTime;

                transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, elapsed);

                yield return null;
            }
            
            // The gameobject that is stored does not include the ragdoll container, solution, just remove the parent instead.
            Destroy(gameObject.transform.parent.gameObject);
            if (hasPlayerModel)
            {
                Destroy(playerModel);
            }
            yield return null;
        }
    }
}
